import qs from 'qs';

export const getUsers = queryString =>
  fetch(`${process.env.REACT_APP_BASE_API_URL}/users?${qs.stringify(queryString)}`)
    .then(response => response.json());

import React, { useState, useRef, useReducer } from 'react';
import styled from 'styled-components/macro';

import Input from '../../components/Input';

import {
  reducer,
  initialState,
} from './reducer';

const THRESHOLD = 0.25;

const Wrapper = styled.div`
  position: relative;
  & > * {
    margin: 4px 0;
  }
`;

const withAutoComplete = (ListComponent, effects) => {
  const AutoComplete = ({ value, onClick, ...props }) => {
    const [search, setSearch] = useState(value);
    const [state, dispatch] = useReducer(reducer, initialState);
    const [{ fetch, fetchMore }] = useState(effects(dispatch));
    const searchTimeout = useRef();

    function handleChange(e) {
      const searchValue = e.target.value;
      setSearch(searchValue);
      clearTimeout(searchTimeout.current);
      searchTimeout.current = setTimeout(
        () => fetch(searchValue, state.pagination.page, state.pagination.perPage),
        600,
      );
    }

    function handleScroll(e) {
      const threshold = e.target.scrollHeight * (1 - THRESHOLD);
      const scrollPosition = e.target.clientHeight + e.target.scrollTop;

      if (!state.pagination.isFetching && !state.pagination.didInvalidate && scrollPosition > threshold) {
        fetchMore(search, state.pagination.page + 1, state.pagination.perPage, state.pagination.isLastPage);
      }
    }


    return (
      <Wrapper>
        <Input {...props} onChange={handleChange} value={search} />
        <ListComponent
          onScroll={handleScroll}
          onClick={onClick}
          list={state.data}
          isLoading={state.isFetching}
          hasError={state.didInvalidate}
          nextIsLoading={state.pagination.isFetching}
          nextHasError={state.pagination.didInvalidate}
          nextIsFinished={state.pagination.isLastPage}
          error={state.error}
        />
      </Wrapper>
    );
  }

  AutoComplete.defaultProps = {
    ListComponent: styled.div,
    value: '',
  };

  return AutoComplete;
};

export default withAutoComplete;

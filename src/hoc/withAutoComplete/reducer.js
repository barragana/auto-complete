import {
  FETCH_REQUESTED,
  FETCH_SUCCESSED,
  FETCH_FAILED,
  PAGE_FETCH_SUCCESSED,
  PAGE_FETCH_FAILED,
  PAGE_FETCH_REQUESTED,
} from '../../actionTypes';

export const initialState = {
  isFetching: false,
  didInvalidate: false,
  data: [],
  pagination: {
    isFetching: false,
    didInvalidate: false,
    page: 1,
    perPage: 10,
    isLastPage: false,
  },
};

export function reducer(state, { type, payload }) {
  switch (type) {
    case FETCH_REQUESTED:
      return {
        ...initialState,
        isFetching: true,
        pagination: {
          ...initialState.pagination,
        },
      };
    case FETCH_SUCCESSED:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        data: payload.data,
        pagination: {
          ...initialState.pagination,
        },
      };
    case FETCH_FAILED:
      return {
        ...initialState,
        isFetching: false,
        didInvalidate: true,
        error: payload.error,
      };
    case PAGE_FETCH_REQUESTED:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          isFetching: true,
          didInvalidate: false,
          page: state.pagination.page + 1,
        },
      }
    case PAGE_FETCH_SUCCESSED:
      return {
        ...state,
        data: state.data.concat(payload.data),
        pagination: {
          ...state.pagination,
          isFetching: false,
          isLastPage: payload.data < state.pagination.perPage,
        },
      }
    case PAGE_FETCH_FAILED:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          isFetching: false,
          didInvalidate: true,
          error: payload.error,
        },
      }
    default:
      throw new Error();
  }
}

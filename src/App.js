import React from 'react';
import styled from 'styled-components/macro';

import withAutoComplete from './hoc/withAutoComplete/withAutoComplete';
import GithubUserList from './components/GithubUserList/GithubUserList';

import effects from './effects';

const StyledApp = styled.div`
  align-items: center;
  display: flex;
  height: 100vh;
  justify-content: center;
  padding: 20px;
  position: relative;
  width: 100vw;
`;

const Form = styled.div`
  max-width: 600px;
  width: 100%;
`;

const GithubUserAutoComplete = withAutoComplete(GithubUserList, effects);

function App() {
  return (
    <StyledApp>
      <Form>
        <GithubUserAutoComplete placeholder="Search github user" onClick={user => alert(`You clicked on ${user.login}`)} />
      </Form>
    </StyledApp>
  );
}

export default App;

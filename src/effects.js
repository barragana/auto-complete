import { getUsers } from './api/users';
import {
  FETCH_REQUESTED,
  FETCH_SUCCESSED,
  FETCH_FAILED,
  PAGE_FETCH_SUCCESSED,
  PAGE_FETCH_FAILED,
  PAGE_FETCH_REQUESTED,
} from './actionTypes';

const effects = dispatch => {
  const fetch = (q, page, per_page) => {
    if (!q) return dispatch({ type: FETCH_SUCCESSED, payload: { data: [] } });

    dispatch({ type: FETCH_REQUESTED });

    getUsers({ q, page, per_page })
      .then(data => {
        dispatch({ type: FETCH_SUCCESSED, payload: { data: data.items } });
      })
      .catch(e => {
        dispatch({ type: FETCH_FAILED, payload: { error: e } });
      });
  };

  const fetchMore = (q, page, per_page, ended) => {
    dispatch({ type: PAGE_FETCH_REQUESTED });
    getUsers({ q, page, per_page })
      .then(data => {
        dispatch({ type: PAGE_FETCH_SUCCESSED, payload: { data: data.items } });
      })
      .catch(e => {
        dispatch({ type: PAGE_FETCH_FAILED, payload: { error: e } });
      });
  };

  return {
    fetch,
    fetchMore,
  };
}

export default effects;

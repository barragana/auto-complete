import colors from './colors';

export const borderWidth = {
  xs: '1px',
  s: '2px',
  m: '4px',
  l: '6px',
  xl: '8px',
};

export const borderRadius = {
  s: '2px',
  m: '4px',
  l: '8px',
};

export const presets = {
  default: `${borderWidth.xs} solid ${colors.n300}`,
};

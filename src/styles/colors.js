export const neutral = {
  n100: '#F9FAFB',
  n200: '#EEF0F4',
  n300: '#CFD0D5',
  n400: '#C0C1C6',
  n500: '#868A96',
  n600: '#656A76',
  n700: '#545862',
  n800: '#3D3F46',
  n900: '#222428',
  n1000: '#0E0E10',
};

export default {
  white: '#FFFFFF',
  black: neutral.n1000,
  error: '#CD4543',
  ...neutral,
};

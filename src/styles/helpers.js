import { css } from 'styled-components';

import { centerOptions } from './constants';

export const withCenterV = ({ center }) =>
  center === centerOptions.V &&
  css`
    align-items: center;
    height: 100%;
  `;
export const withCenterH = ({ center }) =>
  center === centerOptions.H &&
  css`
    justify-content: center;
    width: 100%;
  `;
export const withCenter = ({ center }) =>
  center &&
  css`
    display: flex;
  `;

import styled from 'styled-components/macro';

const sizes = {
  S: '24px',
  M: '40px',
};

const Avatar = styled.img`
  border-radius: 50%;
  height: ${props => sizes[props.size]};
  width: ${props => sizes[props.size]};
`;

Avatar.sizes = {
  S: 'S',
  M: 'M',
};

Avatar.defaultProps = {
  size: Avatar.sizes.M,
};

export default Avatar;

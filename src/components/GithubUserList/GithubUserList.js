import React from 'react';
import styled, { css } from 'styled-components/macro';
import PropTypes from 'prop-types';

import GithubUserListItem from './GithubUserListItem';
import LoadingIndicator from '../Indicators/LoadingIndicator';
import ErrorIndicator from '../Indicators/ErrorIndicator';

const withPadding = ({ theme, noPadding }) =>
  !noPadding &&
  css`
    padding: ${theme.spacing.xs} ${theme.spacing.s};
  `;

const outerWrapperStyle = ({ theme, noPadding }) => css`
  border: ${theme.border.presets.default};
  border-radius: ${theme.border.borderRadius.m};
  box-shadow: ${theme.boxShadows.presets.default};
  max-height: 200px;
  overflow-y: auto;
  position: absolute;
  width: 100%;
`;

const OuterWrapper = styled.div`
  ${outerWrapperStyle};
  ${withPadding};
`;

const InnerWrapper = styled.div`
  overflow-y: auto;
`;

const StyledErrorIndicator = styled(ErrorIndicator)`
  left: 0;
  position: absolute;
`;

const GithubUserListContent = ({
  className,
  list,
  isLoading,
  hasError,
  nextIsLoading,
  nextHasError,
  nextIsFinished,
  error,
  ...props
}) => {
  if (isLoading) {
    return (
      <LoadingIndicator center={LoadingIndicator.centerOptions.H} size={LoadingIndicator.sizes.XS} />
    );
  }

  if (hasError) {
    return (
      <ErrorIndicator center={LoadingIndicator.centerOptions.H}>
        Something went wrong
      </ErrorIndicator>
    );
  }

  return (
    <>
      <InnerWrapper>
        {list.map((user, i) => <GithubUserListItem key={`${i}_${user.node_id}`} user={user} {...props} />)}
      </InnerWrapper>
      {!nextIsFinished &&
        nextIsLoading && (
          <LoadingIndicator center={LoadingIndicator.centerOptions.H} size={LoadingIndicator.sizes.XS} />
        )}
      {nextHasError && (
        <StyledErrorIndicator center={ErrorIndicator.centerOptions.H}>
          Something went wrong
        </StyledErrorIndicator>
      )}
    </>
  )
};

const GithubUserList = ({ className, onScroll, ...props }) =>
  (props.isLoading ||
  props.hasError ||
  (props.list && !!props.list.length)) && (
  <OuterWrapper className={className} onScroll={onScroll} noPadding={props.hasError}>
    <GithubUserListContent {...props} />
  </OuterWrapper>
);

GithubUserList.propTypes = {
  className: PropTypes.string,
  list: PropTypes.arrayOf(GithubUserListItem.propTypes.user).isRequired,
};

export default GithubUserList;

import React from 'react';
import styled from 'styled-components/macro';
import PropTypes from 'prop-types';

import Avatar from '../Avatar';

const Wrapper = styled.div`
  align-items: center;
  display: flex;
  cursor: pointer;
  padding: ${({ theme }) => theme.spacing.xs};
  vertical-align: middle;
`;

const Column = styled.div`
  display: flex;
  margin: ${({ theme }) => `0 ${theme.spacing.xxs}`};

  &:first-child {
    margin-left: 0;
  }

  &:last-child {
    border-right: 0;
  }
`;

const GithubUserListItem = ({ user, onClick, ...props }) => (
  <Wrapper onClick={() => onClick(user)} {...props}>
    <Column>
      <Avatar src={user.avatar_url} alt="avatar" size={Avatar.sizes.S} />
    </Column>
    <Column>
      <span>{user.login}</span>
    </Column>
  </Wrapper>
);

GithubUserListItem.propTypes = {
  user: PropTypes.shape({
    avatar_url: PropTypes.string.isRequired,
    login: PropTypes.string.isRequired,
  }),
  onClick: PropTypes.func.isRequired,
}

export default GithubUserListItem;
